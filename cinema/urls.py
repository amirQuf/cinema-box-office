from rest_framework.routers import SimpleRouter

from .views import FilmViewSet ,ShowtimeViewSet , TheaterViewSet


router = SimpleRouter()

router.register('film',FilmViewSet)
router.register('showtime',ShowtimeViewSet)
router.register('theater',TheaterViewSet)



urlpatterns = []

urlpatterns += router.urls