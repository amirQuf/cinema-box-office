from django.db import models
from django.utils import timezone

class Film(models.Model):
    name = models.CharField(max_length=20)
    description = models.TextField()
    director = models.CharField(max_length=20)
    release_date = models.DateTimeField(default = timezone.now )
    image =  models.ImageField(upload_to='Film')
    is_Show =models.BooleanField(default = False) 
    def __str__(self):
        return self.name

class Genre(models.Model):
    name = models.CharField(max_length=20)
    film = models.ManyToManyField(Film)

class  Theater(models.Model):
    name = models.CharField(max_length=70)
    capcity = models.PositiveIntegerField(default=0)
    number = models.PositiveIntegerField(default=0)
    def __str__(self):
        return self.name

class Showtime(models.Model):
    film = models.ForeignKey(Film , on_delete=models.CASCADE)
    theater = models.ForeignKey(Theater, on_delete=models.CASCADE)
    start_time = models.DateTimeField(default = timezone.now )
    price = models.PositiveIntegerField(default=0)