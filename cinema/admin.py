from django.contrib import admin

from .models import Theater ,Film ,Showtime ,Genre

admin.site.register(Film)
admin.site.register(Genre)
admin.site.register(Showtime)
admin.site.register(Theater)