from rest_framework.viewsets import ModelViewSet

from .models import Film, Showtime, Theater
from .serializers import FilmSerializer, ShowtimeSerializerReadOnly, TheaterSerializer

from rest_framework.permissions import IsAuthenticated , AllowAny
from .permissions import  IsAdmin

class FilmViewSet(ModelViewSet):
    queryset= Film.objects.all()
    serializer_class = FilmSerializer

    def get_permissions(self):
        if self.action in ['list','detail']:
            permission_classes = [AllowAny]
        else:
            permission_classes = [IsAuthenticated,IsAdmin]
        return [permission() for permission in permission_classes]




class TheaterViewSet(ModelViewSet):
    queryset= Theater.objects.all()
    serializer_class = TheaterSerializer

    def get_permissions(self):
        if self.action in ['list','detail']:
            permission_classes = [AllowAny]
        else:
            permission_classes = [IsAuthenticated,IsAdmin]
        return [permission() for permission in permission_classes]




class ShowtimeViewSet(ModelViewSet):
    queryset= Showtime.objects.all()
    serializer_class = ShowtimeSerializerReadOnly
    
    def get_permissions(self):
        if self.action in ['list','detail']:
            permission_classes = [AllowAny]
        else:
            permission_classes = [IsAuthenticated,IsAdmin]
        return [permission() for permission in permission_classes]

