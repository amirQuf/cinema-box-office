from rest_framework.viewsets import ModelViewSet
from .serializers import TicketSerializer
from .models import Ticket
from rest_framework.permissions import IsAuthenticated

class TicketViewSet(ModelViewSet):
    queryset = Ticket.objects.all()
    serializer_class = TicketSerializer
    permission_classes = [IsAuthenticated,]
    
