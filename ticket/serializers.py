from rest_framework.serializers import ModelSerializer
from .models import Ticket
from cinema.serializers import ShowtimeSerializerReadOnly

class TicketSerializer(ModelSerializer):
    showtime =ShowtimeSerializerReadOnly()
    class Meta:
        model = Ticket
        fields = '__all__'

