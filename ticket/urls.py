from rest_framework.routers import SimpleRouter

from .views import TicketViewSet


router = SimpleRouter()

router.register('ticket',TicketViewSet)


urlpatterns = []

urlpatterns += router.urls