from django.db import models
from cinema.models import Showtime
from account.models import User

class Disscount(models.Model):
    title = models.CharField(max_length = 5 , unique = True)
    description = models.CharField(max_length = 100, null =True , blank = True)
    is_active = models.BooleanField()
    created = models.DateTimeField(auto_now_add=True)

class Ticket(models.Model):
    showtime = models.ForeignKey(Showtime , on_delete=models.CASCADE)
    user = models.ForeignKey(User , on_delete=models.CASCADE)
    date = models.DateTimeField(auto_now_add = True)
    quantity=models.PositiveIntegerField(default=0)
    modified = models.DateTimeField(auto_now=True)
    disscount = models.ForeignKey(Disscount, on_delete=models.CASCADE)


