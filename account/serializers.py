
from abc import ABC

from django import utils
from django.contrib.auth import authenticate, get_user_model
from django.contrib.auth.tokens import (PasswordResetTokenGenerator,
                                        default_token_generator)
from django.contrib.sites.shortcuts import get_current_site
from django.core.mail import send_mail
from django.utils.encoding import force_str
from django.utils.http import urlsafe_base64_decode as uid_decoder
from rest_framework import serializers
from rest_framework.exceptions import ValidationError
from rest_framework_simplejwt.tokens import RefreshToken

from .models import User

user_model = get_user_model()


class RegisterSerializer(serializers.ModelSerializer):
    def create(self, validated_data):
        print(validated_data)
        user = super().create(validated_data)
        user.is_active = True
        user.set_password(validated_data['password'])
        user.save()
        return user

    class Meta:
        model = User
        extra_kwargs = {
            'password': {'write_only': True},
        }
        read_only_fields = ('id',)
        fields = ['id', 'username', 'first_name', 'email', 'password',
                  'last_name', 'is_active', 'status']


class UserSerializer(serializers.ModelSerializer):
    role = serializers.CharField(source='get_status_display')

    class Meta:
        model = User
        fields = ['id', 'username', 'first_name', 'email',
                  'last_name', 'is_active', 'status', 'role']


class LoginSerializer(serializers.Serializer):
    username = serializers.CharField(required=False)
    password = serializers.CharField(max_length=128)

    def validate(self, attrs):
        username = attrs.get('username')
        password = attrs.get('password')
        user = authenticate(username=username, password=password)
        # user = User.objects.filter(username=username).first()
        if user:
            # making token for user
            refresh = RefreshToken.for_user(user)
            attrs['user'] = user
            attrs['refresh'] = str(refresh)
            attrs['access'] = str(refresh.access_token)
        else:
            raise ValidationError('user is not exist')

        return attrs


class PasswordResetSerializer(serializers.Serializer):
    email = serializers.EmailField()

    def validate(self, attrs):
        email = attrs.get('email')
        try:
            user = User.objects.get(email=email)
            base64_encoded_id = utils.http.urlsafe_base64_encode(
                utils.encoding.force_bytes(user.id))
            token = PasswordResetTokenGenerator().make_token(user)
            current_site = get_current_site(self.context['request'])
            reset_url = f'{current_site}/reset/{base64_encoded_id}/{token}'
            email_body = reset_url
            send_mail(
                'Reset Password',
                email_body,
                'from@example.com',
                [user.email, ],
                fail_silently=False,
            )
        except User.DoesNotExist:
            raise ValidationError('user is not exist')
        return attrs


class PasswordResetConfirmSerializer(serializers.Serializer):
    new_password = serializers.CharField(max_length=128)

    def create(self, validated_data):
        new_password = validated_data.get('new_password')
        self.request.user.set_password(new_password)
        return self.request.user


class PasswordChangeSerializer(serializers.Serializer):
    old_password = serializers.CharField(max_length=128)
    new_password = serializers.CharField(max_length=128)

    def validate(self, attrs):
        old_password = attrs.get('old_password')
        new_password = attrs.get('new_password')
        request = self.context['request']
        valid = request.user.check_password(old_password)
        if not valid:
            raise ValidationError('incorect password')
        else:
            request.user.set_password(new_password)

        return super().validate(attrs)