from rest_framework.permissions import BasePermission, SAFE_METHODS

from .models import User

class IsAuthorOrReadOnly(BasePermission):

    def has_object_permission(self, request, view, obj):
        if request.method in SAFE_METHODS:
            return True

        return request.user == obj

class IsAdmin(BasePermission):
    def has_permission(self, request, view):
        if request.user.is_anonymous:
            return  False
        return request.user.status == User.ADMIN



class Is_Vertified(BasePermission):
    def has_permission(self, request, view):
        if request.user.is_anonymous:
            return  False
        return (request.user.status == User.ADMIN and 
        request.user.is_vertifed== True)
