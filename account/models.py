from django.db import models


from django.contrib.auth.models import AbstractUser


class User(AbstractUser):
    ADMIN = 1
    REGULAR = 2

    ROLES = (
        (ADMIN, 'Admin'),
        (REGULAR, 'Regular'),
    )
    is_vertifed = models.BooleanField(default= False)
    status = models.SmallIntegerField(choices=ROLES, default=REGULAR)
    mobile = models.CharField(max_length=11 , unique=True)
