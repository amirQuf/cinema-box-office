from django.utils.translation import gettext as _
from rest_framework import exceptions, status
from rest_framework.generics import CreateAPIView, GenericAPIView, ListAPIView
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from .models import User
from .permissions import IsAuthorOrReadOnly, IsAdmin
from .renderers import CustomRenderer
from .serializers import (LoginSerializer, RegisterSerializer, PasswordChangeSerializer,
                          PasswordResetConfirmSerializer,
                          PasswordResetSerializer, UserSerializer)
from rest_framework.permissions import IsAuthenticated


class RegisterView(CreateAPIView):
    serializer_class = RegisterSerializer
    permission_classes = (AllowAny,)
    # renderer_classes = [CustomRenderer ]


class UserViewSet(ModelViewSet):
    serializer_class = UserSerializer
    queryset = User.objects.all()
    permission_classes = (IsAuthorOrReadOnly,)
    # renderer_classes = [CustomRenderer,]


class SearchUser(ListAPIView):
    serializer_class = UserSerializer
    queryset = User.objects.all()

    def list(self, request, *args, **kwargs):
        search = request.query_params.get('search')
        queryset = User.objects.filter(username__contains=search)
        if not queryset:
            return Response({'type': 'error', 'message': 'user not Found.'}, status=status.HTTP_404_NOT_FOUND)
        serializer = UserSerializer(data=queryset, many=True)
        serializer.is_valid()
        return Response({'type': 'success', 'result': serializer.data
                         })


class LoginView(GenericAPIView):
    serializer_class = LoginSerializer
    permission_classes = (AllowAny,)

    def post(self, request):
        try:

            serializer = LoginSerializer(data=request.data)
            serializer.is_valid(raise_exception=True)
            user_serializer = UserSerializer(serializer.validated_data['user'])
        except exceptions.ValidationError as e:
            return Response({'type': 'error', 'message': str(e)})

        return Response({'type': 'success',
                         'message': 'user loged in',
                         'result': {
                             'user': user_serializer.data,
                             'refresh': serializer.validated_data['refresh'],
                             'access': serializer.validated_data['access'],
                         }})

class ForgetPasswordView(GenericAPIView):
    serializer_class = PasswordResetSerializer
    # permission_classes = (AllowAny,)

    def post(self, request):
        try:
            serializer = PasswordResetSerializer(
                data=request.data, context={'request': request})
            serializer.is_valid(raise_exception=True)
        except exceptions.ValidationError as e:
            return Response({'type': 'error', 'message': str(e)})

        return Response(
            {'message': _('email has been sent.'),
             'type': 'success'
             })


class PasswordResetConfirmView(GenericAPIView):
    serializer_class = PasswordResetConfirmSerializer
    permission_classes = (IsAuthenticated,)

    def post(self, request):
        try:
            serializer = self.get_serializer(
                data=request.data)
            serializer.request = request
            serializer.is_valid(raise_exception=True)
            serializer.save()
        except exceptions.ValidationError as e:
            return Response({'type': 'error', 'message': str(e)
                             })
        return Response(
            {'message': _(
                'Password has been reset with the new password.'), 'type': 'success', },
        )


class UpdatePasswordView(GenericAPIView):
    serializer_class = PasswordChangeSerializer
    permission_classes = (IsAuthenticated,)

    def post(self, request):
        try:
            serializer = PasswordChangeSerializer(
                data=request.data, context={'request': request})
            serializer.is_valid(raise_exception=True)
        except exceptions.ValidationError as e:
            return Response({'type': 'error', 'message':  str(e)})
        return Response({
            'message': 'password changed',
            'type': 'success',
            'result': {
            }
        })